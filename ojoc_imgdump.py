#!/usr/bin/env python2

# OJOC - An Open JOdel Client
# Copyright (C) 2016  Christian Fibich
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import OJOC.Connection
import json
import os
import sys
import requests
import argparse


def download_post(post, basedir):
    pi = post.get('post_id')
    image_url = post.get('image_url')
    ih = post.get('image_headers')
    if (image_url is None or pi is None):
        return -1

    fp = os.path.join(basedir, post['post_id'] + ".jpg")
    if (os.path.exists(basedir) and not os.path.exists(fp)):
        try:
            print 'reading image: ' + fp
            if (ih is not None):
                r = requests.request('GET', 'https:' + image_url, headers=ih)
            else:
                r = requests.request('GET', 'https:' + image_url)
            if (r.status_code == 200):
                with open(fp, 'wb') as fd:
                    for chunk in r.iter_content(1024):
                        fd.write(chunk)
        except Exception as e:
            print e
            raise

if __name__ == "__main__":

    print """
    OJOC, Copyright (C) 2016 Christian Fibich
    OJOC comes with ABSOLUTELY NO WARRANTY; for details see LICENSE.
    This is free software, and you are welcome to redistribute it
    under certain conditions; see LICENSE for details.
    """

    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--start-over', action='store_true', help='Run in loop')
    parser.add_argument('-o', '--output-dir', nargs=1, help='Where to put the images', metavar='PATH')
    parser.add_argument('-c', '--citycc', nargs=1, help='Your location, e.g. Vienna,AT', metavar='CITY,CC')

    try:
        n = parser.parse_args(sys.argv[1:])
        args = vars(n)
    except:
        sys.exit(1)

    loc = args.get('citycc')

    if (loc is not None):
        print "Location specified as " + loc[0]
        loc = loc[0]

    od = args.get('output_dir')

    if (od is not None):
        od = od[0]
    else:
        od = "."

    if (args.get('start_over')):
        start_over = True
    else:
        start_over = False

    state_filename = os.path.join(od, '.imgdump.state')
    conn = OJOC.Connection.Connection(citycc=loc)
    old_posts = []
    start_over = 0

    if (os.path.exists(state_filename) and os.path.isfile(state_filename) and start_over == 0):
        with open(state_filename, 'r') as f:
            try:
                old_posts = json.load(f)
            except:
                print "No JSON in file: starting over"
    elif (not os.path.exists(od)):
        os.makedirs(od)

    new_post_data = conn.recent_posts()
    new_posts = [p['post_id'] for p in new_post_data['posts']]
    post_id_set = set(old_posts + new_posts)

    try:
        for post_id in post_id_set:
            post = conn.particular_post(post_id)
            if (post is None):
                continue
            download_post(post, od)
            children = post.get('children')
            if (children is None):
                continue
            for child in children:
                rv = download_post(child, od)
    finally:
        post_id_list = list(post_id_set)

        with open(state_filename, 'w') as f:
            old_posts = json.dump(post_id_list, f)

    sys.exit(0)
