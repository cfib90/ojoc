# Jacob Karlsson #

* Give responders the numbers they actually use

# Dominic S #

* Correct enum34 package prerequisite

# @jangarcia #

* HMAC Key for 4.21.1
* HMAC retrieval method after 4.19.1

# @infotoni91

* Location Selection in GUI

# @Loewe1000 and @Kian1991

* Captcha filename <==> Solution map

# @Nils Borrman

* APK download script, PostDetails API endpoint RE

