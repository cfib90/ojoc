#!/usr/bin/env python2

# OJOC - An Open JOdel Client
# Copyright (C) 2016  Christian Fibich
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import urwid
import enum
import OJOC.Connection
import OJOC.prettytime
import OJOC.Emoji2Text
import random
import subprocess
import argparse
import os
import sys


class PostType(enum.Enum):
    """Enumeration of types of posts which can be displayed by the GUI"""
    ALL_POSTS = 1
    MY_POSTS = 2
    MY_REPLIES = 3
    MY_VOTES = 4
    PARTICULAR_POST = 5
    MY_PINNED_POSTS = 6
    CHANNEL = 7

PostTypeStrings = {PostType.ALL_POSTS: 'All Posts',PostType.MY_POSTS: 'My Posts',PostType.MY_REPLIES: 'My Replies',PostType.MY_VOTES: 'My Votes',PostType.PARTICULAR_POST: 'Post',PostType.MY_PINNED_POSTS: 'My Pinned Posts', PostType.CHANNEL: 'Channel'}


class PostCategory(enum.Enum):
    """Categories to filter posts by the API"""
    RECENT = 1
    COMBO = 2
    POPULAR = 3
    DISCUSSED = 4


class MenuType(enum.Enum):
    """Enumeration of Menus and Buttons"""
    SUBMENU = 1
    VIEWMODE_BUTTON = 2
    ABOUT_BUTTON = 3
    EXIT_BUTTON = 4
    HELP_BUTTON = 5
    CHANNEL_BUTTON = 6


PostCategoryStrings = {PostCategory.RECENT: 'Recent',PostCategory.COMBO: 'Combo',PostCategory.POPULAR: 'Popular',PostCategory.DISCUSSED: 'Discussed'}


colors = ['06A3CB', '8ABDB0', 'DD5F5F', '9EC41C', 'FF9908', 'FFBA00']

palette = [ ('shadow',            'black',         'black', '', '#000', '#000'),
            ('button normal',     'black',         'light gray', 'standout'),
            ('button select',     'white',         'brown',      '', '#fff', '#f90'),
            ('dialog content',    'black',         'light gray'),
            ('dialog background', 'dark gray',     'light gray'),
            ('06A3CB', 'white',   'dark cyan',     '',           '#fff', '#09C'),
            ('06A3CB reverse',    'dark cyan',     'white', '', '#09C', '#fff'),
            ('06A3CB on black',   'dark cyan',     'black', '', '#09C', '#000'),
            ('8ABDB0', 'white',   'dark blue',     '', '#666', '#9C9'),
            ('8ABDB0 reverse',    'dark blue',     'white', '', '#9C9', '#666'),
            ('8ABDB0 on black',   'dark blue',     'white', '', '#9C9', '#000'),
            ('DD5F5F', 'white',   'dark red',      '', '#fff', '#c66'),
            ('DD5F5F reverse',    'dark red',      'white', '', '#c66', '#fff'),
            ('DD5F5F on black',   'dark red',      'white', '', '#c66', '#000'),
            ('9EC41C', 'white',   'dark green',    '', '#fff', '#9C3'),
            ('9EC41C reverse',    'dark green',    'white', '', '#9C3', '#fff'),
            ('9EC41C on black',   'dark green',    'white', '', '#9C3', '#000'),
            ('FF9908', 'white',   'light magenta', '', '#fff', '#F90'),
            ('FF9908 reverse',    'light magenta', 'white', '', '#F90', '#fff'),
            ('FF9908 on black',   'light magenta', 'white', '', '#F90', '#000'),
            ('FFBA00', 'white',   'brown',         '', '#666', '#FC0'),
            ('FFBA00 reverse',    'brown',         'white', '', '#FC0', '#666'),
            ('FFBA00 on black',   'brown',         'white', '', '#FC0', '#000'),
]


menubar_tags = [('F2', 'View'), ('F5', 'Refresh'), ('F6', 'Application')]

menues = [('f2', u'View...',
            [(MenuType.SUBMENU, u'All Posts',
                [(MenuType.VIEWMODE_BUTTON, u'Combo', (PostType.ALL_POSTS, PostCategory.COMBO)),
                 (MenuType.VIEWMODE_BUTTON, u'Popular', (PostType.ALL_POSTS, PostCategory.POPULAR)),
                 (MenuType.VIEWMODE_BUTTON, u'Discussed', (PostType.ALL_POSTS, PostCategory.DISCUSSED))]),
             (MenuType.SUBMENU, u'My Posts', [
                (MenuType.VIEWMODE_BUTTON, u'All', (PostType.MY_POSTS, PostCategory.COMBO)),
                (MenuType.VIEWMODE_BUTTON, u'Popular', (PostType.MY_POSTS, PostCategory.POPULAR)),
                (MenuType.VIEWMODE_BUTTON, u'Discussed', (PostType.MY_POSTS, PostCategory.DISCUSSED))]),
            (MenuType.VIEWMODE_BUTTON, u'My Replies', (PostType.MY_REPLIES, PostCategory.RECENT)),
            (MenuType.VIEWMODE_BUTTON, u'My Votes', (PostType.MY_VOTES, PostCategory.RECENT)),
            (MenuType.VIEWMODE_BUTTON, u'My Pinned Posts', (PostType.MY_PINNED_POSTS, PostCategory.RECENT)),
            (MenuType.SUBMENU, u'Channel',
                [(MenuType.CHANNEL_BUTTON, u'Recent', PostCategory.RECENT),
                 (MenuType.CHANNEL_BUTTON, u'Popular', PostCategory.POPULAR),
                 (MenuType.CHANNEL_BUTTON, u'Discussed', PostCategory.DISCUSSED)])]),
        ('f6', u'Application', [(MenuType.ABOUT_BUTTON, u'About', None),(MenuType.HELP_BUTTON, u'Help', None),(MenuType.EXIT_BUTTON, u'Quit', None)
    ])
]


class MenuButton(urwid.WidgetPlaceholder):
    """Button to be used in a menu"""

    def __init__(self, caption, callback, arg):
        button = urwid.Button(caption)
        urwid.connect_signal(button, 'click', callback, arg)
        widget = urwid.AttrMap(button, 'button normal', 'button select')
        super(MenuButton, self).__init__(widget)


class SubMenu(MenuButton):
    """A MenuButton which opens a submenu"""

    def __init__(self, top, caption, choices):

        self.top = top
        self.contents = Menu(top, caption, choices)

        super(SubMenu, self).__init__([caption, u'...'], self.open_menu, None)

    def open_menu(self, button):
        return toplevel_widget.open_box(self.contents)


class Menu(urwid.WidgetPlaceholder):
    """The base class for menus"""

    def __init__(self, top, title, choices):

        body = [urwid.AttrMap(urwid.Text(title),'dialog content'),urwid.Divider('-')]

        for choice in choices:
            if choice[0] == MenuType.SUBMENU:
                body.append(SubMenu(top, choice[1], choice[2]))
            elif choice[0] == MenuType.VIEWMODE_BUTTON:
                body.append(MenuButton(choice[1],top.set_view_mode,choice[2]))
            elif choice[0] == MenuType.CHANNEL_BUTTON:
                body.append(MenuButton(choice[1],top.set_channel,choice[2]))
            elif choice[0] == MenuType.ABOUT_BUTTON:
                body.append(MenuButton(choice[1], top.about_cb, choice[2]))
            elif choice[0] == MenuType.HELP_BUTTON:
                body.append(MenuButton(choice[1], top.help_cb, choice[2]))
            elif choice[0] == MenuType.EXIT_BUTTON:
                body.append(MenuButton(choice[1],top.exit_program_cb,choice[2]))

        super(Menu, self).__init__(urwid.ListBox(urwid.SimpleFocusListWalker(body)))

    def mouse_event(self, size, event, button, col, row, focus):
        if button == 4:
            self.keypress(size, 'up')
        elif button == 5:
            self.keypress(size, 'down')
        else:
            super(Menu,self).mouse_event(size,event,button,col,row,focus)


class About(urwid.WidgetPlaceholder):
    """The about dialog widget"""

    def __init__(self, top):
        body = [urwid.AttrMap(urwid.Text(t),'dialog content') for t in ["OJOC, (C) 2016 Christian Fibich","OJOC comes with absolutely no warranty; for details see LICENSE."
                "This is free software, and you are welcome to redistribute it"
                "under certain conditions; see LICENSE for details.",
                "",
                "OJOC makes use of OpenStreetMap's Nominatim Service.",
                "See http://wiki.openstreetmap.org/wiki/Nominatim for details."
                ]]

        body.append(urwid.Divider('-'))
        body.append(MenuButton(u'Ok', top.close_menu, None))
        super(About, self).__init__(urwid.Filler(urwid.Pile(body)))

class AskChannel(urwid.WidgetPlaceholder):
    """The channel input widget"""

    def __init__(self, top, ok_callback):
        self.ok_callback = ok_callback
        self.entry = urwid.Edit(caption="Channel> ")
        body = [self.entry,urwid.Divider('-'),MenuButton(u'Ok', self._ok_callback, top)]
        super(AskChannel, self).__init__(urwid.Filler(urwid.Pile(body)))
    
    def _ok_callback(self,wid,top):
        self.ok_callback(top,self.entry.get_edit_text())

class Help(urwid.WidgetPlaceholder):
    """The help dialog widget"""

    def __init__(self, top):
        body = [urwid.AttrMap(urwid.Text(t), 'dialog content') for t in
                [" F2  --  Select which posts are shown"," F5  --  Reload the current category"," F6 --  This menu"," /   --  Filter current category"]]

        body.append(urwid.Divider('-'))
        body.append(MenuButton(u'Close', top.close_menu, None))
        super(Help, self).__init__(urwid.Filler(urwid.Pile(body)))

class Error(urwid.WidgetPlaceholder):
    """The error dialog widget"""

    def __init__(self, top, text):
        if type(text) is list:
           body = [urwid.AttrMap(urwid.Text(t), 'dialog content') for t in text]
        elif type(text) is str:
           body = [urwid.AttrMap(urwid.Text(t), 'dialog content') for t in [text]]

        body.append(urwid.Divider('-'))
        body.append(MenuButton(u'OK', top.close_menu, None))
        super(Error, self).__init__(urwid.Filler(urwid.Pile(body)))


class Post (urwid.WidgetPlaceholder):
    """A widget which contains a single post with its content and
       metadata, as well as buttons to perform actions (Delete if own
       upvote and downvote)"""

    def __init__(self, this_post, top, ispost=True):

        self.top = top
        self.conn = top.conn

        self.message = message = OJOC.Emoji2Text.translate(this_post['message'])

        text = urwid.Text((this_post['color'], message))
        location = urwid.Text((this_post['color'],this_post['location']['name'] +
                ' (' +
                self.__named_distance__(this_post['distance']) +
                ')'),align='center')
        votes = urwid.Text((this_post['color'], str(this_post['vote_count'])), align='center')
        num_children = this_post.get('child_count')
        if (num_children is None):
            num_children = 0

        if top.view_mode[0] != PostType.PARTICULAR_POST:
            open_button = urwid.Button('Reply (' + str(num_children) + ')')
            urwid.connect_signal(open_button,'click',self.top.set_view_mode,(PostType.PARTICULAR_POST,None,this_post['post_id']))
        else:
            open_button = urwid.Text('')

        voted = this_post.get('voted')

        if (voted is None):
            upvote_button = urwid.Button('Up')
            downvote_button = urwid.Button('Down')
            urwid.connect_signal(upvote_button,'click',self.upvote,this_post['post_id'])
            urwid.connect_signal(downvote_button,'click',self.downvote,this_post['post_id'])
        elif voted == 'up':
            upvote_button = urwid.Text('Up')
            downvote_button = urwid.Text('')
        else:
            upvote_button = urwid.Text('')
            downvote_button = urwid.Text('Down')

        if ispost:
            pinned = this_post.get('pinned')
            pin_count = this_post.get('pin_count')

            if pin_count is None:
                pin_count_text = ''
            else:
                pin_count_text = ' (%d)' % pin_count

            if (pinned is None):
                pin_button = urwid.Button("Pin"+pin_count_text)
                urwid.connect_signal(pin_button,'click',self.pin,this_post['post_id'])
            else:
                pin_button = urwid.Button("Unpin"+pin_count_text)
                urwid.connect_signal(pin_button,'click',self.unpin,this_post['post_id'])
        else:
            pin_button = urwid.Text('')


        time = urwid.Text(OJOC.prettytime.pretty(this_post['updated_at']))

        if this_post['post_own'] == 'own':
            delete_post_button = urwid.Button('Delete')
            urwid.connect_signal(delete_post_button,'click',self.delete_post_cb,this_post['post_id'])
        else:
            delete_post_button = urwid.Text('')

        buttons = [('weight',b[0],urwid.AttrMap(b[1],this_post['color'],this_post['color'] +
                 ' reverse')) for b in [(1,open_button),(1,pin_button),(1,delete_post_button),(3,location),(1,time),(1,votes),(1,upvote_button),(1,downvote_button)]]

        image_url = this_post.get('image_url')
        if image_url is not None:
            open_button = urwid.Button('Open Image')
            urwid.connect_signal(open_button,'click',self.open_image,'\nhttps://' +
                image_url)
            open_button_attr = urwid.AttrMap(open_button,this_post['color'],this_post['color'] +
                ' reverse')
            content = [text, open_button_attr, urwid.Columns(buttons)]
        else:
            content = [text, urwid.Columns(buttons)]

        super(Post,self).__init__(urwid.AttrMap(urwid.LineBox(urwid.Pile(content)),this_post['color']))

    def __named_distance__(self, distance):
        """Map the numerical distance provided by the API to a String"""
        if (distance < 2):
            return "very near"
        elif (distance < 15):
            return "near"
        else:
            return "remote"

    def open_image(self, wid, arg):
        """If possible, display an image URL using firefox"""
        with open(os.devnull, 'w') as FNULL:
            try:
                subprocess.call(["firefox", arg], stdout=FNULL,stderr=subprocess.STDOUT)
            except OSError:
                # We might be on a terminal without X
                pass

    def pin(self, wid, arg):
        """Pin a post.
           arg contains the postid"""
        try:
            self.top.conn.pin(arg)
            self.top.refresh()
        except OJOC.Connection.ConnectionError as e:
            self.top.error_dialog(str(e))

    def unpin(self, wid, arg):
        """Unpin a post.
           arg contains the postid"""
        try:
            self.top.conn.unpin(arg)
            self.top.refresh()
        except OJOC.Connection.ConnectionError as e:
            self.top.error_dialog(str(e))

    def upvote(self, wid, arg):
        """Upvote a post.
           arg contains the postid"""
        try:
            self.top.conn.upvote(arg)
            self.top.refresh()
        except OJOC.Connection.ConnectionError as e:
            self.top.error_dialog(str(e))

    def downvote(self, wid, arg):
        """Downvote a post.
           arg contains the postid"""
        try:
            self.top.conn.downvote(arg)
            self.top.refresh()
        except OJOC.Connection.ConnectionError as e:
            self.top.error_dialog(str(e))

    def delete_post_cb(self, button, data):
        """Delete a post.
           arg contains the postid"""
        try:
            self.conn.delete_post(data)
            self.top.refresh()
        except OJOC.Connection.ConnectionError as e:
            self.top.error_dialog(str(e))


class Editor(urwid.WidgetPlaceholder):
    """A widget which edits a new post. Should be packed at the very
       end of a list of posts.
       The prompt is changed if it is a reply"""

    def __init__(self, top, color, ancestor=None):
        """top       the top widget
           color     the color of this post
           ancestor  the postid of the original post to be answered.
                     Omit if new post to be edited"""
        self.top = top
        self.ancestor = ancestor
        send = urwid.Button('Send')
        if (ancestor is None):
            caption = 'New Jodel: '
        else:
            caption = 'Jodel back: '

        edit = urwid.Edit(caption=caption, multiline=True)
        data = (edit, (ancestor, color))

        urwid.connect_signal(send, 'click', self.new_post_cb, data)
        buttons = [urwid.AttrMap(b, color, color + ' reverse') for b in [send]]

        super(Editor, self).__init__(urwid.AttrMap(urwid.LineBox(urwid.Pile([urwid.AttrMap(edit, color, color + ' reverse'), urwid.Columns(buttons)])), color))

    def new_post_cb(self, button, data):
        """Callback to send the new post to the API"""
        arg = data[1]
        try:
            self.top.conn.new_post(data[0].get_edit_text(), arg[1], None, self.ancestor, '')
            self.top.refresh()
        except OJOC.Connection.ConnectionError as e:
            self.top.error_dialog(str(e))


def exit_program(button):
    raise urwid.ExitMainLoop(button)


class MenuBox(urwid.WidgetPlaceholder):
    """The Menu toplevel widget."""

    max_box_levels = 4

    def __init__(self, menubar_tags, menues, conn):
        """menubar_tags     A list of shortcut keys and names to be displayed on the topbar
           menues           Menu dict as defined by the global menues variable
           conn             OJOC.Connection object to communicate with the API"""

        self.conn = conn

        menubar_widgets = []
        for menubar_tag in menubar_tags:
            menubar_widgets.append(urwid.Columns([('pack', urwid.Text(('button select', menubar_tag[0]))), ('pack', urwid.Text(('button normal', menubar_tag[1])))], dividechars=1))

        self.menues = []

        for menu in menues:
            self.menues.append((menu[0], Menu(self, menu[1], menu[2])))

        self.search = False
        self.box_level = 0
        self.hotkey = None
        self.frame = urwid.Frame(urwid.SolidFill(),urwid.AttrMap(urwid.Columns(menubar_widgets),'button normal'),urwid.Text(''))
        self.set_view_mode(None, (PostType.ALL_POSTS, PostCategory.COMBO))
        self.filter = urwid.Edit(caption='Filter: ')

        super(MenuBox, self).__init__(self.frame)

    def open_box(self, box):
        """Overlay a menu box over the currently displayed widgets"""
        self.original_widget = urwid.Overlay(urwid.AttrMap(urwid.LineBox(box),'dialog background'),self.original_widget,align='center',width=('relative',40),valign='middle',height=('relative',40),min_width=24,min_height=12,left=self.box_level *
            3,right=(self.max_box_levels -
                self.box_level -
                1) *
            3,top=self.box_level *
            2,bottom=(self.max_box_levels -
                self.box_level -
                1) *
            2)
        self.box_level += 1

    def close_menu(self, arg):
        """Close one layer of menues"""
        self.original_widget = self.frame
        self.box_level = 0

    def keypress(self, size, key):
        """Handles the following keypresses

           When in filter mode:
           esc    Exit filter mode
           enter  Use currently entered word as filter

           When in normal mode:
           esc    Closes the uppermost layer menu currently displayed
           f5     Refresh the current view
           /      Enter filter mode

           Other keypresses are forwarded to the Super class"""

        if self.search:
            if key == 'esc':
                self.search = False
                self.__set_footer()
                self.frame.set_focus('body')
            elif key == 'enter':
                filter_word = self.filter.get_edit_text()
                num = self.frame.contents['body'][0].filter(filter_word)
                self.search = False
                self.__set_footer({'filter_word': filter_word, 'num': num})
                self.frame.set_focus('body')
            else:
                return super(MenuBox, self).keypress(size, key)
        else:
            if key == 'esc' and self.box_level > 0 or key == self.hotkey and self.box_level == 1:
                self.hotkey = None
                self.original_widget = self.original_widget[0]
                self.box_level -= 1
            elif key == 'f5':
                self.refresh()
            elif key == '/':
                self.frame.footer = urwid.AttrMap(self.filter, 'dialog content', 'dialog background')
                self.frame.set_focus('footer')
                self.search = True
            else:
                for menu in self.menues:
                    if key == menu[0]:
                        self.hotkey = menu[0]
                        self.open_box(menu[1])
                        return None
                return super(MenuBox, self).keypress(size, key)

    def set_view_mode(self, button, arg):
        """Update the view mode.
           arg is a tuple of (PostType and PostCategory)
           it defines which types of posts (My Jodels, My Replies, ...) and
           which category (Recent, Popular, ...) is shown."""
        self.view_mode = arg
        self.refresh()
        self.close_menu(button)

    def set_channel(self, button, arg):
        """Update the view mode and channel.
           arg is the PostCategory defining which category (Recent, Popular, ...) is shown."""

        self.view_mode = (PostType.CHANNEL, arg)
        self.close_menu(button)
        self.open_box(AskChannel(button,self.channel_cb))

    def refresh(self):
        """Reload the current view"""
        self.__set_footer()
        self.frame.contents['body'] = (PostList(self), None)

    def channel_cb(self,widget,channel):
        self.channel = channel
        self.refresh()
        self.close_menu(widget)

    def error_dialog(self,text):
        """Display an error dialog"""
        self.open_box(Error(self,text))

    def about_cb(self, widget):
        """Display the about dialog"""
        self.open_box(About(self))

    def help_cb(self, widget):
        """Display the help dialog"""
        self.open_box(Help(self))

    def exit_program_cb(self, widget):
        """Close the program cleanly"""
        exit_program(None)

    def __set_footer(self, search_result=None):
        """Update the footer row depending on the view mode and search mode.
           karma, category/post ID and location are displayed outside search mode."""
        try:
            karma = self.conn.karma()
            text = 'Karma: ' + str(karma['karma'])
        except OJOC.Connection.ConnectionError as e:
            self.top.error_dialog(str(e))
            text = 'Karma: N/A'

        category = PostTypeStrings[self.view_mode[0]]
        if search_result is not None:
            ptype = ' -- "' + \
                search_result['filter_word'] + '" (' + str(search_result['num']) + ')'
        else:
            if self.view_mode[0] is PostType.CHANNEL:
                ptype = ' #'+self.channel + ' -- '+PostCategoryStrings[self.view_mode[1]]
            else:
                if self.view_mode[1] is not None:
                    ptype = ' -- ' + PostCategoryStrings[self.view_mode[1]]
                else:
                    ptype = ' -- ' + self.view_mode[2]

        location = self.conn.location['city'] + \
            ',' + self.conn.location['country']
        text_widgets = [urwid.Text(t[0], align=t[1]) for t in [(text, 'left'), (category + ptype, 'center'), (location, 'right')]]
        self.frame.contents['footer'] = (urwid.AttrMap(urwid.Columns(text_widgets),'dialog content'),None)


class PostList(urwid.ListBox):
    """ListBox widget containing and displaying a list of posts"""

    def __init__(self, top):

        post_type = top.view_mode[0]
        post_category = top.view_mode[1]

        # fetch posts according to the selected posttype and category
        try:
            if post_type == PostType.ALL_POSTS:
                if post_category == PostCategory.POPULAR:
                    posts = top.conn.popular_posts()
                elif post_category == PostCategory.DISCUSSED:
                    posts = top.conn.discussed_posts()
                elif post_category == PostCategory.COMBO:
                    rawdata = top.conn.combo_posts()
                    if rawdata is not False:
                        posts = {'posts': sorted(rawdata['replied'] +
                                rawdata['voted'] +
                                rawdata['recent'],key=timesort,reverse=True)}
                    else:
                        posts = None
            elif post_type == PostType.MY_POSTS:
                if post_category == PostCategory.COMBO:
                    posts = top.conn.my_combo_posts()
                elif post_category == PostCategory.POPULAR:
                    posts = top.conn.my_popular_posts()
                elif post_category == PostCategory.DISCUSSED:
                    posts = top.conn.my_discussed_posts()
            elif post_type == PostType.MY_REPLIES:
                posts = top.conn.my_replies()
            elif post_type == PostType.MY_VOTES:
                posts = top.conn.my_voted_posts()
            elif post_type == PostType.MY_PINNED_POSTS:
                posts = top.conn.my_pinned_posts()
            elif post_type == PostType.CHANNEL:
                rawdata = top.conn.get_channel(top.channel)
                if rawdata is not False:
                    if post_category == PostCategory.RECENT:
                        posts = {'posts': rawdata.get('recent')}
                    elif post_category == PostCategory.POPULAR:
                        posts = {'posts': rawdata.get('popular')}
                    elif post_category == PostCategory.DISCUSSED:
                        posts = {'posts': rawdata.get('discussed')}
                else:
                    posts = None
        except OJOC.Connection.ConnectionError as e:
            self.top.error_dialog(str(e))
            posts = None

        post_widgets = []

        # Add Post widgets for the fetched posts to the list
        if post_type == PostType.PARTICULAR_POST:
            try:
                rawdata = top.conn.particular_post(top.view_mode[2])
                post_widgets.append(Post(rawdata, top))
                children = rawdata.get('children')
                if children is not None:
                    for c in children:
                        post_widgets.append(Post(c, top, ispost=False))
                post_widgets.append(Editor(top,rawdata['color'],rawdata['post_id']))
            except OJOC.Connection.ConnectionError as e:
                self.top.error_dialog(str(e))

        elif posts is not False and \
            posts is not None and \
            posts.get('posts') is not None:
            for p in posts['posts']:
                post_widgets.append(Post(p, top))

        if len(post_widgets) == 0:
            post_widgets.append(urwid.AttrMap(urwid.Text('Could not get posts.\nReload to try again.'),'FF9908 on black'))
        elif post_type == PostType.ALL_POSTS:
            post_widgets.append(Editor(top,colors[random.randint(0,len(colors) -
                            1)]))

        self.lw = urwid.SimpleFocusListWalker(post_widgets)

        super(PostList, self).__init__(self.lw)

    def mouse_event(self, size, event, button, col, row, focus):
        """Handle the mousewheel for easy scrolling"""
        if button == 4:
            self.keypress(size, 'up')
        elif button == 5:
            self.keypress(size, 'down')
        else:
            super(PostList,self).mouse_event(size,event,button,col,row,focus)

    def filter(self, word):
        """Filter the content according to the given filter word"""
        not_matching_widgets = []
        word = word.lower()
        not_matching_widgets = [post_widget for post_widget in self.lw if isinstance(post_widget, Post) and post_widget.message.lower().find(word) == -1]

        for not_matching_widget in not_matching_widgets:
            self.lw.remove(not_matching_widget)

        if isinstance(self.lw[-1], Editor):
            return len(self.lw) - 1
        else:
            return len(self.lw)


def timesort(post):
    return int(post['post_id'], 16)


def splash_jodel():
    """Display a splash screen."""
    # This is a joke towards Jodel Venture because they lock us out
    # of their API
    lines = ["MMMMMMMMMMMMMMMI.........   .$MMMMMMMMMMMMMMMM","MMMMMMMMMMMD+  :?DNMMMMMMMN8?, .INMMMMMMMMMMMM","MMMMMMMMMN   8MMMMMMMMMMMMMMMMM$. :8MMMMMMMMMM","MMMMMMMM  +MMMMMMMMMMMMMMMMMMMMMMM . MMMMMMMMM","MMMMMM7. OMMMMMMMMMMMMMMMMMMMMMMMMMN .NMMMMMMM","MMMMM? .MMM7 .~MMMMMMMMMMMMMMMZ~.,MMM. DMMMMMM","MMMM~ ~MMMM?    ~NMMMMMMMMMNI    .MMMD  OMMMMM","MMMN. MMMMM7        .. ...       =MMMMM. MMMMM","MMM:.ZMMMMMO                     $MMMMM: $MMMM","MMM.7MMMMMMM~                   ~MMMMMMM,.8MMM","MMM.DMMMMMMN:  INMMM$~ =ZMMMD=  ?MMMMMMMI IMMM","MMM.MMMMMMM. DMMMN =MMMMM..MMMM$ .MMMMMMO ~MMM","MMM.MMMMMM,  .NMM=D MMMMM$7MMMO   IMMMMM8 ~MMM","MMM.MMMMMN     ?MMMMMMMMMMMMM~     $MMMM7 ?MMM","MMM.ZMMMMI      .7MM~   +MM?       :MMMM~ ZMMM","MMM..NMMMMMMZ                   MMMMMMMI +MMMM","MMM$.,MMMMMMN                   MMMMMMM  MMMMM","MMMM. OMMMMMMMMMD:         +DMMMMMMMMM~ =MMMMM","MMMMN  IMMMMMMMMMMMM7?+?$MMMMMMMMMMMM. +MMMMMM","MMMMMM..:MMMMMMMMMMMMMMMMMMMMMMMMMMM. +MMMMMMM","MMMMMMM7.,MMMMMMMMMMMMMMMMMMMMMMMMZ  NMMMMMMMM","MMMMMMMMM? ,?MMMMMMMMMMMMMMMMMMM:  ,MMMMMMMMMM","MMMMMMMMMMN.  :MMMMMMMMMMMMMMD   .MMMMMMMMMMMM","MMMMMMMMMMMMMNI   ..,:~,.     ZMMMMMMMMMMMMMMM","MMMMMMMMMMMMMMMMM8+~.   .=IDMMMMMMMMMMMMMMMMMM","","You made it that far! Unfortunately our API","isn't public (yet).","[https://api.go-tellm.com/api/v2]"]

    return urwid.AttrMap(urwid.Filler(urwid.Pile([urwid.Text(('FF9908 on black', l), align='center') for l in lines])),'shadow')


def splash(top):
    """Display another splash screen"""

    license = [s.strip() for s in OJOC.Config.SPLASH_TEXT.split('\n')]
    lines = ["  .oooooo.      oooo   .oooooo.     .oooooo.        o8o  .ooooo.     .oooo.   "," d8P'  `Y8b     `888  d8P'  `Y8b   d8P'  `Y8b       `YP 888' `Y88.  d8P'`Y8b  ","888      888     888 888      888 888                '  888    888 888    888 ","888      888     888 888      888 888                    `Vbood888 888    888 ","888      888     888 888      888 888                         888' 888    888 ","`88b    d88'     888 `88b    d88' `88b    ooo               .88P'  `88b  d88' "," `Y8bood8P'  .o. 88P  `Y8bood8P'   `Y8bood8P'             .oP'      `Y8bd8P'  ","             `Y888P                                                           ","","OJOC - An Open JOdel Client"," Copyright (C) 2016 Christian Fibich",""]+license

    logo = [urwid.Text((colors[random.randint(0,len(colors) -
                    1)] +
             ' on black',l),align='center') for l in lines]
    startb = urwid.Button('Start')
    exitb = urwid.Button('Exit')
    urwid.connect_signal(startb, 'click', start_cb, top)
    urwid.connect_signal(exitb, 'click', exit_program, None)
    logo.extend([urwid.Divider(),('pack',urwid.AttrMap(startb,'FF9908 on black','FF9908')),('pack',urwid.AttrMap(exitb,'FF9908 on black','FF9908'))])

    return urwid.AttrMap(urwid.Filler(urwid.Pile(logo)),'shadow')


def start_cb(widget, top):
    """Show the top widget when called"""
    loop.widget = top


def init_cb(widget, conn):
    """Generate the MenuBox toplevel widget which contains the entire
       GUI when called"""
    global toplevel_widget
    toplevel_widget = MenuBox(menubar_tags,menues,conn)
    loop.widget = splash(toplevel_widget)

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('-c','--citycc',nargs=1,help='Your location, e.g. Vienna,AT',metavar='CITY,CC')

    try:
        n = parser.parse_args(sys.argv[1:])
        args = vars(n)
    except TypeError as e:
        print str(e)
        sys.exit(1)

    loc = args.get('citycc')

    if (loc is not None):
        print "Location specified as " + loc[0]
        loc = loc[0]

    try:
        conn = OJOC.Connection.Connection(citycc=loc)
    except Exception as e:
        print "Could not create connection object: "+str(e)
        sys.exit(-1)

    random.seed()
    parent = splash_jodel()
    toplevel_widget = None
    # Show the first splash seceen
    loop = urwid.MainLoop(splash_jodel(), palette)
    try:
        loop.screen.set_terminal_properties(colors=16)
        loop.screen.set_terminal_properties(colors=256)
    except:
        print "Setting colors failed. Falling back"
    # Show the second splash screen and start the execution
    loop.set_alarm_in(2, init_cb, conn)
    loop.run()
