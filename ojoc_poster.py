#!/usr/bin/env python2

# OJOC - An Open JOdel Client
# Copyright (C) 2016  Christian Fibich
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import OJOC.Connection
import json
import os
import signal
import sys
import select
import requests
import datetime
import copy
import json
import dateutil.parser
import dateutil.tz as tz
import time
import argparse
import re
import os
import subprocess

alltime_posts = {}
latest_posts = {}
geodump_prefix = None
lexical_prefix = None
verbose = False


def print_verbose(string):
    if verbose:
        print string

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--citycc', nargs=1, help='Your location, e.g. Vienna,AT', metavar='CITY,CC')
    parser.add_argument('-', '--stdin', action='store_true', help='Read from stdin')
    parser.add_argument('-f', '--file', nargs=1, help='File containing a post', metavar='FILE')
    parser.add_argument('-i', '--image', nargs=1, help='Attach an image', metavar='IMAGE')
    parser.add_argument('-d', '--device-uid', nargs=1, help='Specify the device UID (useful for scripting)', metavar='DEVICE-UID')
    parser.add_argument('-a', '--ancestor', nargs=1, help='Specify the ancestor ID', metavar='ANCESTOR-ID')
    parser.add_argument('-v', '--verbose', action='store_true', help='Verbose output')

    print_verbose("""
    OJOC, Copyright (C) 2016 Christian Fibich
    OJOC comes with ABSOLUTELY NO WARRANTY; for details see LICENSE.
    This is free software, and you are welcome to redistribute it
    under certain conditions; see LICENSE for details.
    """)

    try:
        n = parser.parse_args(sys.argv[1:])
        args = vars(n)
    except:
        sys.exit(1)

    loc = args.get('citycc')
    fn = args.get('file')
    uid = args.get('device_uid')
    image = args.get('image')
    ancestor = args.get('ancestor')

    if (args.get('verbose')):
        verbose = True
    else:
        verbose = False

    if (loc is not None):
        print_verbose("Location specified as " + loc[0])
        loc = loc[0]

    if (uid is not None):
        print_verbose("UID specified as " + uid[0])
        uid = uid[0]

    if (image is not None):
        print_verbose("Image specified: " + image[0])
        image = image[0]

    if (ancestor is not None):
        print_verbose("Answering to post: " + ancestor[0])

    if (args.get('stdin')):
        stdin = True
    else:
        stdin = False

    conn = OJOC.Connection.Connection(citycc=loc, uid=uid)


# --- POST

    if (stdin == False):

        if (fn is None):
            filepath = '.post'

            with open(filepath, 'w') as f:
                f.write('# Edit your post below\n')
                f.write('# All lines starting with \'#\' will be ignored\n')

        # --- EDIT

            if sys.platform.startswith('darwin'):
                subprocess.call(('open', filepath))
            elif os.name == 'nt':
                os.startfile(filepath)
            elif os.name == 'posix':
                editor = os.getenv('EDITOR')
                if editor is None:
                    editor = 'nano'
                subprocess.call((editor, filepath))
        else:
            filepath = fn[0]

        with open(filepath, 'r') as f:
            lines = [line for line in f.readlines() if line[0] != '#']
    else:
        print "\n\nEnter your post below."
        print "All lines starting with \'#\' will be ignored...\n"
        lines = [line for line in sys.stdin.read().split('\n') if (len(line) > 0 and line[0] != '#')]

    if (len(lines) == 0 or ("\n>".join(lines)).isspace()):
        print_verbose("Post message empty")
        sys.exit(3)
    else:
        print_verbose("\n")
        if (image is not None):
            with open(image, 'r') as img:
                image = img.read()
                resp = conn.new_post('\n'.join(lines), image_data=image, ancestor=ancestor)
        else:
            resp = conn.new_post('\n'.join(lines))

    for post in resp['posts']:
        if post['post_own'] == "own" and post['message'] == '\n'.join(lines):
            print post['post_id']
            sys.exit(0)

    sys.exit(1)
